public class Main {
    public static void main(String[] args) {
        KrestikiNoliki.StartGame();
        do{
            KrestikiNoliki.inputPlayer();
            KrestikiNoliki.BoardAnalysis();
            KrestikiNoliki.PrintGameBoard();
            if (KrestikiNoliki.STATUS_VICTORY_X == KrestikiNoliki.GameStat){
                System.out.println("'X' победил!");
            } else if (KrestikiNoliki.STATUS_VICTORY_O == KrestikiNoliki.GameStat){
                System.out.println("'O' победил!");
            } else if (KrestikiNoliki.GameStat== KrestikiNoliki.STATUS_DRAW){
                System.out.println("Игра закончилась ничьей!");
            }
             KrestikiNoliki.ActivePlayer = ( KrestikiNoliki.ActivePlayer== KrestikiNoliki.SIGN_X?KrestikiNoliki.SIGN_X:KrestikiNoliki.SIGN_O);
        }
        while (KrestikiNoliki.GameStat== KrestikiNoliki.STATUS_GAME_CONTINUES);
    }

}